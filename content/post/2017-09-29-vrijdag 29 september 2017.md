---
title: Eindelijk een nieuw idee
subtitle:
date: 2017-09-29
tags:

---
Eindelijk weer een goed idee! Dit maakte mij heel erg blij vooral toen wij als groep eindelijk een idee hadden.

 Ik zat de hele week vast en ik kon niks verzinnen, ik ging weer twijfelen. de gedachten gingen weer in mijn hoofd van: "Ga ik het wel halen?" 

 dus ik was erg blij dat we als groep een idee konden verzinnen. dit geeft mij een goed gevoel en daardoor kwam de zin weer in mijn hoofd: "I can do this!"
