---
title: Nog meer spelanalyses
subtitle:
date: 2017-09-26
tags:

---
Vandaag heb ik mij bezig gehouden met het maken van de spelanalyses van spellen die je kan koppelen met bouwkunde.
Ik had gekozen voor Minecraft en Yenga, en mijn groep besloot om ook een spelanalyse te maken van Age of Empires.

Aangezien ik het maken van spelanalyses heel erg leuk vind heb ik de taak opgenomen om alle drie de spelanalyses te maken.

Nadat ik klaar was met de spelanalyses ben ik samen met mijn groep ideeën gaan bedenken voor ons volgende spel.
Vanna bedacht om eens helemaal niet aan online, ofline en bouwkunde te denken. Hierdoor kwamen wij op ideeën en dat gaf weer een fijn gevoel vol met motivatie.

Dankzij het samen werken heb ik geleerd dat het soms het beste is om even non-topic te denken.
