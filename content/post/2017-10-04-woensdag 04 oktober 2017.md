---
title: Emoties
subtitle:
date: 2017-10-04
tags:

---
Vandaag is ons spel weer gepresenteerd, we hebben feedback gekregen en daar gaan we zeker wat mee doen. Iteratie 3 komt eraan, dit betekend dat we het project binnenkort gaan afsluiten.

Toch had ik pijn in mijn buik: ik was bang dat ik het nog steeds niet goed deed, ik was boos omdat we geen duidelijke informatie krijgen.

Uiteindelijk hadden wij nog een uurtje een soort studiecoaching uur. Dit was het moment dat ik Elbert ontmoeten. Hij gaf een zeer goede les over gevoelens. Wanneer ben je nou bang? Wanneer ben je nou boos? Wanneer ben je nou verdrietig? Het aapje op je schouder aan een ander geven.

Dit inspireerde mij heel erg ik ging anders na denken over gevoelens en wat er allemaal bij komt kijken.

Dankzij deze les ging ik toch wat meer gemotiveerd naar huis, I can do this kwam weer in mijn hoofd. We gaan knallen, we gaan dit kwartaal halen!
