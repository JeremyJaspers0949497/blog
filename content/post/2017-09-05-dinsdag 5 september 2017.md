---
title: De eerste studio dag
subtitle:
date: 2017-09-05
tags:

---
Nou daar zijn we dan, vandaag mijn eerste studio dag gehad.

Afgelopen weekend hebben ik een spel idee bedacht voor Design Challenge 1. Toen ik dit voorlegde merkte ik dat ik de opdracht verkeerd begrepen had. Gelukkig kon mijn team mij uitleggen wat er fout was aan mijn idee.

Nadat ik feedback op mijn idee had gekregen begonnen wij aan het bedenken over welke doelgroep en welk thema wij zouden gebruiken. In het begin moest ik aardig aan mijn groep wennen, omdat iedereen van een andere opleiding afkwam en daarom ook allemaal een verschillende achtergrond hadden.

Nadat ik feedback had gevraagd aan een tweede jaars student kwam ik samen met mijn groep al op een idee voor een thema, doelgroep en een spel.
Ik heb alle ideeën getypt in een word document, samen hebben we het doorgelezen en waarnodig aangepast.

En vandaag ben ik begonnen met het schrijven van mijn blog, en dit zal best wel wennen zijn om hem bij te houden.

Het belangerijkste wat ik heb geleerd vandaag is dat iedereen andere ideeën en meningen heeft, en dat je door rekening met elkaar houden best ver kan komen.

![alt tekst](../Images/teamwerk.png)
