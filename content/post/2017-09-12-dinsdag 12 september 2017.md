---
title: De derde studiodag
subtitle:
date: 2017-09-12
tags:

---
Derde studiodag gehad vandaag. Samen met mijn groep besloten om de doelgroep te veranderen, dit omdat het heel moeilijk was onze huidige doelgroep te contacten.

Ook heb ik mee geholpen om ideeën te bedenken voor ons eerste paper prototype en heb ik een flowchart gemaakt voor de schermen van de app.
