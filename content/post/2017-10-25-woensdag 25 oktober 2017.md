---
title: De Expo.. en het einde.
subtitle:
date: 2017-10-25
tags:

---
Vandaag was het dan ook zo ver, de expo. Ons spel presenteren, feedback verzamelen en punten ontvangen.

Zelf heb ik een halfuur gepresenteerd, toch vond ik het best lastig om het spel uit mijn hoofd uit te leggen. Maar de mensen die ik gepitch heb begrepen het spel goed.

Ik heb ook nog verder gekeken bij andere spellen en het zag er allemaal super leuk uit. Soms dacht ik "Waarom ben ik daar niet op gekomen?" Maar goed toch wel een van de leuke dingen van de opleiding.

Tijdens het praten met mensen van andere groepen ontstond er toch weer verwarring bij het leerdossier. Gelukkig kwam Elbert nog even voor een soort studiecoaching en probeerde onze vragen te beantwoorden zodat wij met een goed gevoel naar huis konden gaan.

Op het einde moesten wij elkaar feedback geven over hoe de samenwerking was en de punten waar je nog aan moet werken in de ogen van andere mensen.
De teamgenoten die mij hebben beoordeeld waren goed positief, toch kwamen ze allemaal op het punt initiatie tonen. In mijn ogen heb ik best initiatief getoont, iniedergeval dat doe ik meestal wel bijvoorbeeld op mijn werk.

Maargoed, misschien hebben ze toch wel een beetje gelijk. Iniedergeval kan ik ervoor zorgen dat ik meer laat zien dat ik initiatief toon aan mijn volgende groep.

Op het einde heb ik iedereen van mijn groep bedankt voor de fijne samenwerking. Ik heb een leuke tijd met mijn eerste projectgroep gehad, en zeker veel van hun allemaal geleerd.

Op het moment voel ik me nog super gemotiveerd, Ik ga er voor zorgen dat ik mijn leerdossier morgen avond kan inleveren en dan ben ik klaar voor de eindbespreking.

I CAN DO THIS!
