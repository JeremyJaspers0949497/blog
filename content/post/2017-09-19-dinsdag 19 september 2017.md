---
title: Kill your darling
subtitle:
date: 2017-09-19
tags:

---
Vandaag hebben we dan onze eerste presentatie gehad. Zelf heb ik niet gepresenteerd maar ik was wel super trots op ons groepje en het spel dat wij bedacht hadden.

Nadat we terug kwamen van de pauze kregen wij de briefing van iteratie 2. Het begin van deze iteratie heet "Kill your darling".

Dit was een aardige tegenslag, trots op het idee dat we bedacht hadden en ik wou het zo graag verder uitwerken. Maarja blijkbaar moet je als designer je oude concept en ideeën uit het raam kunnen gooien en opnieuw kunnen beginnen.

De rest van de studio dag heb ik niet veel gedaan, wel hebben we de taken verdeeld en ga ik weer verder waar we begonnen zijn.
