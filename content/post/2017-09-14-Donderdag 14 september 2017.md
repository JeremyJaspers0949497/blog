---
title: Het Paper prototype
subtitle:
date: 2017-09-14
tags:

---
Vandaag was best een lastige dag voor mij met veel punten waar ik tegen aanliep.

Omdat wij bang waren dat we in tijdsnood zouden komen te zitten zijn we na de keuzenvakken samengekomen en begonnen aan het Paper prototype.

We probeerde een begin te maken aan het prototype. Ik had al een aantal ideeën gegeven hoe we het aan zouden kunnen pakken, toch twijfelde mijn groep heel erg. Toen leerde ik Vanna beter kennen. Die werd aardig boos op ons groepje dat we gewoon een beslissing moesten maken en het moesten uitvoeren.

Dus dat gingen wij doen. Mijn groepje wou hun eerste paper prototype al gelijk kwaliteit geven, waardoor ik mezelf een beetje buitengesloten voelde. Dit komt omdat ik erg veel moeite heb met tekenen.

Uiteindelijk heb ik Vanna geholpen met het maken van de schermen die we zouden gebruiken voor de app.

Toch ga ik het anders aanpakken met het volgende prototype, dit gaat me niet meer gebeuren!
