---
title: Producten DC1
subtitle: Al mijn gemaakte Producten uit Design Challenge 1
comments: false
---
**Moodboard bouwkunde:**
![alt tekst](../img/moodboard-bouwkunde.png)

**Collage onderzoek-visueel:**
![alt tekst](../img/Collage-onderzoek-visueel.png)

**Eerste spelanalyse:**
![alt tekst](../img/spelanalyse-een.png)

**Presentatie**
samen gemaakt met Vanna
![alt tekst](../img/presentatie.png)

**Planning en taakverdeling**
samen gemaakt met Vanna
![alt tekst](../img/taakverdeling.png)

**spelanalyse Touwtjes springen**
samen gemaakt met Vanna
![alt tekst](../img/spelanalyse-touwtjes-springen.png)

**spelanalyse Starwars Galaxy of Heroes**
samen gemaakt met Lotte
![alt tekst](../img/spelanalyse-goh.png)

**spelanalyse Age of Empires**
![alt tekst](../img/spelanalyse-aoe.png)

**spelanalyse Minecraft**
![alt tekst](../img/spelanalyse-mc.png)

**spelanalyse Yenga**
![alt tekst](../img/spelanalyse-yenga.png)

**Prototype**
![alt tekst](../img/prototype2.png)

**Wireframes**
![alt tekst](../img/wireframes.png)

**spelanalyse Damain**
![alt tekst](../img/spelanalyse-demain.png)

**Toekomst Erasmusbrug**
![alt tekst](../img/toekomst-erasmusbrug.png)

**Toekomst kubus woningen**
![alt tekst](../img/toekomst-cubus.png)
