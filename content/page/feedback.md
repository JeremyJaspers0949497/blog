---
title: Feedback DC1
subtitle: Alle feedback over mijn opdrachten in Design Challenge 1
comments: false
---

**Feedback Moodboard doelgroep :**
![alt tekst](../img/feedback-Moodboard doelgroep.png)

**Feedback Feedback collage :**
![alt tekst](../img/feedback-collage.png)

**Feedback Planning :**
![alt tekst](../img/feedback-planning1.png)

**Feedback spelanalyse 1:**
![alt tekst](../img/feedback-spelanalyse1.png)

**Feedback Presentatie 1:**
![alt tekst](../img/feedback-presentage.png)

**Feedback Iteratie 2:**
![alt tekst](../img/feedback-iteratie2.png)
