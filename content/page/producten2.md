---
title: Producten DC2
subtitle: Al mijn gemaakte Producten uit Design Challenge 2
comments: false
---
**Team invertarisatie :**
![alt tekst](../img/inventarisatie.png)

**Dit ben ik profiel deel 1 :**
![alt tekst](../img/ditbenikprofiel1.png)

**Dit ben ik profiel deel 2 :**
![alt tekst](../img/ditbenikprofiel2.png)

**Planning via Trello :**
![alt tekst](../img/trello.png)

**Merkanalyse Paard :**
![alt tekst](../img/merkwijzerpaard.png)

**Merkanalyse de selecteur :**
![alt tekst](../img/merkwijzerselecteur.png)

**Merkanalyse Poppodium 013 :**
![alt tekst](../img/merkwijzerpoppodium.png)

**High fidality prototype mail :**
![alt tekst](../img/highprototype.png)

**Onderzoeksplan :**
![alt tekst](../img/onderzoeksplan.png)

**Exopositie DC 2 :**
![alt tekst](../img/expodc2.png)
